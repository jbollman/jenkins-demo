import sys

def hello(jenkins_param):
  
  print("Hello, " + jenkins_param.title())

if __name__ == "__main__":
    hello(sys.argv[1])
